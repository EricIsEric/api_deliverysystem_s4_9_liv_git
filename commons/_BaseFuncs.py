# encoding=utf-8
"""
# 当前文件: _BaseFuncs
# 当前日期: 2021/7/19 11:23
# 当前作者: Eric
"""
import hashlib
import os


def toMd5Str(setStr):
    M = hashlib.md5()
    M.update(setStr.encode("utf8"))
    return M.hexdigest()


def checkFilePath(fileDir):
    """
    智能检测文件路径,可解决从bat文件以及run.py文件运行时相对路径的不同的问题,通用为".."开头\n
    :param fileDir: 传入提取数据文件相对位置
    :return: 从文件运行路径可访问目标文件的路径
    """
    if os.path.isfile(fileDir):
        return fileDir
    else:
        if fileDir[0:2] == "..":
            return fileDir[1:]  # 截取掉第一个"."符号
        else:  # 不是"..",而是"./", 需要把表示返回上一级的"."添加上
            return "." + fileDir

