# encoding=utf-8
"""
# 当前文件: shop
# 当前日期: 2021/7/19 11:21
# 当前作者: Eric
"""

import requests
from configs.config import HOST
from commons.login import Login


class Shop:
    def __init__(self, inToken):
        self.header = {'Authorization': inToken}

    # 1. 列出店铺接口
    def shop_list(self, inBody):
        url = f'{HOST}/shopping/myShop'
        payload = inBody
        resp = requests.get(url, params=payload, headers=self.header)
        return resp.json()

    # 2. 图片上传接口
    def file_upload(self, filePath):
        url = f'{HOST}/file'
        """
        文件对象封装: {'变量名': 文件对象属性}
        {'变量名file': (文件名, open(), 文件类型)}
        """
        fileName = filePath.split('/')[-1]
        fileType = fileName.split('.')[-1]
        payload = {'file': (fileName, open(filePath, 'rb'), fileType)}  # 运行完会直接close
        resp = requests.post(url, files=payload, headers=self.header)
        return resp.json()['data']['realFileName']

    # 3. 编辑店铺接口
    def shop_update(self, inBody, shopID, imgInfo):
        url = f'{HOST}/shopping/updatemyshop'

        # 获取动态数据 , 最好结合用例做判断,来区分异常用例与正常用例,也即正向反向用例
        inBody['id'] = shopID
        inBody['image_path'] = imgInfo
        inBody['image'] = f'/file/getImgStream?fileName={imgInfo}'

        payload = inBody
        resp = requests.post(url, data=payload, headers=self.header)
        return resp.json()


if __name__ == '__main__':
    # 1. 登录
    token = Login().login({'username': 'ma0130', 'password': '79540'}, getToken=True)

    # 2. 列出店铺
    fileId = Shop(token).shop_list({'page': 1, 'limit': 20})['data']['records'][0]['id']
    print(fileId)

    # 3. 文件上传
    res2 = Shop(token).file_upload('../data/shop_image.png')

    # 4. 店铺编辑
    infoTestData = {
        "name": "星巴克新建店",
        "address": "上海市静安区秣陵街道666号路",
        "id": "3269",
        "Phone": "13176876632",
        "rating": "6.0",
        "recent_order_num": 100,
        "category": "快餐便当/简餐",
        "description": "满30减5，满60减8",
        "image_path": "b8be9abc-a85f-4b5b-ab13-52f48538f96c.png",
        "image": "http://121.41.14.39:8082/file/getImgStream?fileName=b8be9abc-a85f-4b5b-ab13-52f48538f96c.png"
    }

    res3 = Shop(token).shop_update(infoTestData, fileId, res2)

    print(res3)
