# encoding=utf-8
"""
# 当前文件: login
# 当前日期: 2021/7/19 11:20
# 当前作者: Eric
"""

import requests
import copy
from commons._BaseFuncs import toMd5Str
from configs.config import HOST
from _tools.decorationBase import show_time

"""
没有逻辑的业务层代码
"""


class Login:
    @show_time
    def login(self, inData, getToken=False):
        """
        封装登录接口,默认返回所有接口数据
        :param inData: 传入接口的数据
        :param getToken: 默认False,返回所有接口数据,为True即单独返回token数据
        :return:
        """
        url = f"{HOST}/account/sLogin"
        inData = copy.copy(inData)  # 不改变原有数据,浅拷贝出来一份,防止密码被多次加密
        inData['password'] = toMd5Str(inData['password'])
        payload = inData
        rsp = requests.post(url, data=payload)

        if getToken:
            return rsp.json()['data']['token']
        else:
            return rsp.json()  # 返回所有的接口数据字典


if __name__ == '__main__':
    res = Login().login({'username': 'ma0130', 'password': '79540'}, getToken=False)
    print(res)

    str1 = "你好"
    # 编码操作
    str2 = str1.encode('utf-8')
    print("编码后的结果>>>", str2)

    # 解码操作
    str3 = str2.decode('gbk')  # 解码与编码的方式不一致,会展示为乱码
    print("错误解码后的结果>>>", str3)
    str4 = str2.decode('utf-8')  # 编码解码方式一致,可解码成功
    print("错误解码后的结果>>>", str4)



