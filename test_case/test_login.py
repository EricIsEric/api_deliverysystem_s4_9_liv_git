# encoding=utf-8
"""
# 当前文件: test_login
# 当前日期: 2021/7/22 9:01
# 当前作者: Eric
--------------------------------------------------------------------------------
#*#*#*# 目前0723直播课程进度:  添加到gitlab在线仓库-Test2 #*#*#*#
--------------------------------------------------------------------------------
"""
import os
import pytest
import allure
from _tools.excelControl_v4 import getExcelData
from _tools.yamlControl import get_yaml_caseData
from commons.login import Login
from _tools.loggerBase import log
import traceback
from commons._BaseFuncs import checkFilePath

"""
进行逻辑处理的测试代码
"""

"""
登录接口已经写好了,
登录的用例数据可以读到了--Excel
接下来可以将接口用例跑起来,看实际与预期的结果是否一致

实现方案:
    1. 使用for循环--把读取到的每一个用例数据给对应的登录接口 (不采用)
    2. 使用现成的自动化测试框架来实现 (采用这种方案)
"""


@allure.epic("点餐系统")  # 项目级别
@allure.feature("登录模块")  # 业务模块级别
class TestLogin:  # 登录的测试类
    # 定义测试方法
    @pytest.mark.parametrize('caseTitle, reqBody, expData', get_yaml_caseData(checkFilePath('../data/caseLogin.yaml')))
    @allure.title('{caseTitle}')
    @allure.story("登录接口")
    @pytest.mark.login
    def test_login(self, caseTitle, reqBody, expData):
        # 1. 发送登录接口以及用例数据传入

        try:
            res = Login().login(reqBody)  # 这里不能添加带有默认值的参数: getToken=False
            # 开始断言,使用实际的返回与预期的结果对比是否一致
            assert res['msg'] == expData['msg']
        except Exception as error:
            log.error(traceback.format_exc())  # 打印错误详细信息
            raise error  # 这种形式处理错误时需要在这里进行抛出,否则错误被处理了,框架会认为没有错误产生,因此需要手动抛出,让框架识别到错误


if __name__ == '__main__':
    pytest.main(['test_login.py', '-s', '--alluredir', '../_reports/tmp', "--clean-alluredir"])
    os.system('allure serve ../_reports/tmp')

"""
allure运行原理:
    1. 先通过pytest框架执行测试用例,生成对应的报告需要的源数据
        xxx.json
    2. 使用allure serve 启动服务,执行这些xxx.json源数据
"""

"""
  分析问题:
  1. 用例执行的结果当前会报错
  2. 找具体失败的原因,查看具体的报错或提示
  3. 报错TypeError: string indices must be integers
  4, 最终发现,inData本身是json字符串,不能用键来取值
  5. 从源头去转格式

  解决方案:
  1. 就近原则,在test_login里修改,加上json.loads(reqBody)
      不推荐,这只改了一个地方,其余接口调用均要这样处理,这个方法治标不治本
  2. 从源头解决,修改get_excel_data()的返回值类型
      推荐方案,可以让根方法实现通用
  3. 回到修改get_excel_data()进行优化
  
  
  场景: 一个测试用例里包含3个接口 a-b-c
  断言分类:
  1. assert
    一旦有1个接口断言失败,后续的接口不会被执行
  2. 希望算a断言失败了,后续的操作也能继续执行
    此时断言可以采用assume进行断言
    需要使用pip install pytest-assume命令进行安装
  """
