# -*- coding: utf-8 -*-
"""
# 当前文件: conftest.py  <固定命名,作用域为当前文件夹包>
# 创建日期: 2021/7/25 23:53 (补7/23)
# 创建作者: Eric
"""
import pytest
from commons.login import Login
from commons.shop import Shop
from configs.config import Uname_Pword
from commons._BaseFuncs import checkFilePath

"""
重点知识点为以下3点:
1. scope的4个参数的含义
    <在预习的项目中有记录>

2. autouse的使用场景:
    1. 很多项目级别的一些必要操作,只要执行自动化运行就得自己运行,比如说,连接环境,检查配置,数据库连接判断,这时就需要设置为True自动调用
    2. 需要程序员根据实际需求来选择调用,或者不同模块的前置不一样,这时就需要设置为False手动调用  <默认>

3. fixture有和没有返回值使用的区别
    如果fixture没有返回值,就是用userfixture
    如果fixture有返回值,就将函数名称直接当参数传入, 这个fixture的函数名就可以代表返回值
"""


@pytest.fixture(scope='session', autouse=True)
def running_init():
    # 下面就是setup初始化操作
    print("------开始外卖项目的接口自动化测试------")

    yield  # 分割--下面的就是teardown的操作
    print("------完成外卖项目的接口自动化测试------")


# -------------------------- 初始化账号登录操作 --------------------------#
@pytest.fixture(scope='session')  # autouse默认为False
def login_init():
    token = Login().login(Uname_Pword, True)
    yield token  # 返回值,同return
    print("-----登录操作初始化完成-----")


# -------------------------- 初始化店铺对象操作 --------------------------#
@pytest.fixture(scope='session')  # autouse默认为False
def shop_init(login_init):
    # 创建店铺实例
    shopObj = Shop(login_init)  # 使用fixture的函数名就等于使用它的返回值
    yield shopObj
    print("-----初始化店铺对象操作完成-----")


# -------------------------- 编辑店铺的初始化操作 --------------------------#
@pytest.fixture(scope='function')  # autouse默认为False
def shop_update_init(shop_init):
    shopId = shop_init.shop_list({'page': 1, 'limit': 20})['data']['records'][0]['id']
    imgInfo = shop_init.file_upload(checkFilePath('../data/shop_image.png'))

    yield shop_init, shopId, imgInfo  # 返回的就是元组格式
    # print("-----编辑店铺的初始化操作完成-----")




