# encoding=utf-8
"""
# 当前文件: test_login
# 当前日期: 2021/7/25 15:40 (补7.25)
# 当前作者: Eric
----------------------------------------------------------------------------------
文件说明: 这是直播的第8次课程
当前进度:
直播进度:
----------------------------------------------------------------------------------
"""
# from commons.shop import Shop
# from commons.login import Login
import os
from _tools.excelControl_v4 import getExcelData
import pytest
import allure
from configs.config import Uname_Pword
from commons.login import Login


# @pytest.mark.usefixtures('running_init')
# @pytest.mark.usefixtures('模块_A')  # fixture模块叠加, 这个后运行
# @pytest.mark.usefixtures('模块_B')  # fixture模块叠加, 这个先运行
@pytest.mark.shop  # 设定mark标签
@allure.epic("点餐系统")
@allure.feature("店铺模块")
@pytest.mark.skipif(Login().login(Uname_Pword)['msg'] != '成功', reason='登录接口存在问题,需要进行检查')
@allure.feature("店铺模块")  # 业务模块级别
class TestShop:

    @pytest.mark.parametrize("caseTitle, reqBody, expData", getExcelData("我的商铺", "listshopping", '标题', '请求参数', '响应预期结果'))
    @pytest.mark.shop_list
    @allure.story("列出店铺")
    @allure.title('{caseTitle}')
    def test_shop_list(self, caseTitle, reqBody, expData, shop_init):
        # 调用店铺的列出接口---做数据驱动
        res = shop_init.shop_list(reqBody)

        # 做断言
        if 'code' in res:
            assert res['code'] == expData['code']
        else:
            assert res['error'] == expData['error']

    # 编辑店铺接口
    @pytest.mark.parametrize("caseTitle, reqBody, expData", getExcelData("我的商铺", "updateshopping", '标题', '请求参数', '响应预期结果'))
    @pytest.mark.shop_update
    @allure.story("更新店铺")
    @allure.title('{caseTitle}')
    # @pytest.mark.skip('不执行shop_update')  # 强制跳过
    def test_shop_update(self, caseTitle, reqBody, expData, shop_update_init):
        # 调用店铺的编辑接口---做数据驱动
        res = shop_update_init[0].shop_update(reqBody, shop_update_init[1], shop_update_init[2])

        # 做断言
        if 'code' in res:
            assert res['code'] == expData['code']
        else:
            assert res['error'] == expData['error']


if __name__ == '__main__':
    pytest.main(['test_shop.py', '-s', '--alluredir', '../_reports/tmp', '--clean-alluredir'])
    os.system('allure serve ../_reports/tmp')

    # '-m'可以实现筛选接口层面的数据.具体草书说明可参考预习项目里的记录
    # 如果要筛选用例层面的筛选,需要在读取Excel时使用runCase列表来指定

    # 另外还有'-k'和'-v'两种模式,主要用于终端命令行执行,具体可参考预习项目的记录,
    # 只能精准到方法,不能精准到用例

    # 跳过执行用例  skip / skipif
    # 可参照预习项目的记录
