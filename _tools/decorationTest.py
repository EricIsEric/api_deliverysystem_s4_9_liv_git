# -*- coding: utf-8 -*-
"""
# 当前文件: decorationTest.py
# 创建日期: 2021/7/31 11:52
# 创建作者: Eric
"""

import time

"""
场景描述: 
    A版本的自动化脚本执行对很多个应的用例功能已经实现
提出需求:
    1. 领导要求计算一下对应用例的执行时间
    2. 尽可能不要改代码
"""

# ----------------自动化脚本--------------------
# def test():
#     print("-----自动化开始执行-----")
#     time.sleep(2)
#     print("-----自动化结束执行-----")


# ----------------自动化脚本--------------------

# -------------方案一开始------------- #
"""
在函数执行的前后增加一个计时器
耗时 = 结束时间 - 开始时间
time.time()
"""

# def test():
#     startTime = time.time()
#     print("-----自动化开始执行-----")
#     time.sleep(2)
#     print("-----自动化结束执行-----")
#     endTime = time.time()
#     print("这个用例执行耗时: ", endTime - startTime)


"""
方案一结果:  方案不合理
    1. 直接修改了原代码,如果很多个测试函数,维护起来很痛苦
    2. 
"""
# -------------方案一j结束------------- #

# -------------方案二开始------------- #
"""
方案描述:
    不修改test函数,但是我另外添加一个函数show_time
    
"""


# def test():
#     print("-----自动化开始执行-----")
#     time.sleep(2)
#     print("-----自动化结束执行-----")
#
#
# def show_time(func):
#     startTime = time.time()
#     func()
#     endTime = time.time()
#     print("这个用例执行耗时: ", endTime - startTime)


"""
问题反馈:
    1. 这个是没有修改test函数, 满足第一个函数
    2. 但改变了test函数的执行调用方式
"""
# -------------方案二结束------------- #

# -------------方案三开始------------- #
"""
梳理了需求:
    1. 不能修改test原函数
    2. 不能修改test的调用方式
    3. 使用装饰器技术实现
解决方案:
    在python语法里,有没有这样的一个功能呢?
    装饰器技术就可以实现这样的需求
    
需要引入一个概念: 闭包
python有3个包: 装包: 函数定义时, test(*args, **kwargs) ---> *args可以装包成元组形式, **kwargs可以装包成字典格式
              解包: 函数调用时, test(*[10,20,30]) ---> 这样就实现了解包,将列表3个值分别传递
              闭包: 
                  在一个函数里,定义另一个函数,内置函数使用外面函数的一个变量
                  外函数的返回值是以内函数的函数名为代表的一个对象
"""


def show_time(func):
    def inner():
        startTime = time.time()
        func()
        endTime = time.time()
        print("这个用例执行耗时: ", endTime - startTime)
    return inner


# 在需要增加新功能的函数前面,添加一句@show_time
@show_time  # "语法糖"的实现,相当于语句test = show_time(test)
def test():
    print("-----自动化开始执行1-----")
    time.sleep(2)
    print("-----自动化结束执行1-----")


@show_time  # "语法糖"的实现,相当于语句test2 = show_time(test2)
def test2():
    print("-----自动化开始执行2-----")
    time.sleep(2)
    print("-----自动化结束执行2-----")


"""
方案三问题反馈:
    1. 实现没修改test原函数
    2. 实现没修改test调用方式
缺点:
    每一个test函数前面都得加一条语句: test = show_time(test)
解决:
    使用语法糖@
    
"""


if __name__ == '__main__':
    # ---------- 方案三调试开始---------- #
    # show_time(test)  # 直接返回一个内置函数对象inner
    # test = show_time(test)  # 相当于:test == inner

    # test = show_time(test)
    test()  # 组合起来之后,这一行就相当于inner()
    test2()
    # ---------- 方案三调试结束---------- #

