# encoding=utf-8
"""
# 当前文件: excelControl
# 当前日期: 2021/7/19 11:50
# 当前作者: Eric
"""
import xlrd
import json

"""
目标: 需要挑选特定用例来执行 (一些或者某个)
方案: 
    就是想根据需求来挑选用例执行
    思路:
        1. ALL全部执行对应的用例
        2. 某个用例执行  001, 004
        3. 连续一段用例  001-006
        4. 混合场景  ['001', '005-008', '012']
"""


def getExcelData(set_SheetName, set_CaseName, runCase=['all'], set_ExcelDir='../data/ApiCases_v2.0.xls', *args):
    resList = []  # 存放Excel读取的结果, 放在函数体内,形成局部变量,每一次调用,这个list都会被清空一次,避免被累加

    # 1. 获取Excel路径
    excelDir = set_ExcelDir

    # 2. 把excel加载到内存 -- open,  formatting_info保持原样式
    workBook = xlrd.open_workbook(excelDir, formatting_info=True)

    # 3. 获取对应的sheet表
    # print(workBook.sheet_names())  # 获取所有的sheet的名称
    workSheet = workBook.sheet_by_name(set_SheetName)

    # 通过用户输入的想获取的列名解析出对应的列下标
    # 用户传入列名字符串,但是代码里处理的是列编号
    # 标题  URL
    colIdx = []  # 存放下标的列表
    for i in args:  # 遍历元组元素
        # 列名在第一行数据里有
        colIdx.append(workSheet.row_values(0).index(i))

# --------------解析用户筛选用例
    runList = []
    if 'all' in runCase:  # 全部执行,不做处理
        runList = workSheet.col_values(0)
    else:
        for one in runCase:
            if '-' in one:
                start, end = one.split("-")
                for i in range(int(start), int(end)+1):
                    runList.append(set_CaseName + f'{i:0>3}')
            else:
                runList.append(set_CaseName + f'{one:0>3}')

    # 开始模糊匹配第0列数据
    rowsNum = 0  # 遍历变量表示行数
    for one in workSheet.col_values(0):
        if set_CaseName in one and one in runList:
            getColData = []
            for num in colIdx:
                tmp = workSheet.cell(rowsNum, num).value
                getColData.append(tmp)
            resList.append(getColData)

        rowsNum += 1
    return resList


def fillInResult(set_ExcelDir, set_SheetName, *args):
    workBook = xlrd.open_workbook(set_ExcelDir, formatting_info=True)
    workSheet = workBook.sheet_by_name(set_SheetName)
    print(f"##########{workSheet.cell(1, 12).value}")
    workSheet.cell(1, 12).value = "Pass"


def fillInResult_v2(set_ExcelDir, set_SheetName, set_Rows):
    workBook = xlrd.open_workbook(set_ExcelDir, formatting_info=True)
    workSheet = workBook.sheet_by_name(set_SheetName)
    print(workSheet.cell(set_Rows, 12).value)
    workSheet.cell(1, 12).value = "TEST"


if __name__ == '__main__':
    res = getExcelData("../data/ApiCases_v1.0.xls", "登录模块", "Login", "用例编号", "标题", "URL", runCase=['001', '003-005', '006'])
    for items in res:
        print(items)
