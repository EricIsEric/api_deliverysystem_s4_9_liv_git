# -*- coding: utf-8 -*-
"""
# 当前文件: loggerBase.py
# 创建日期: 2021/8/1 22:54
# 创建作者: Eric
"""
import logging
from commons._BaseFuncs import checkFilePath

"""
日志相关内容:
    1. 日志的输出渠道: 文件形式(xxx.log)还是控制台形式输出
    2. 日志级别: DEBUG,INFO,WARNING,ERROR,CRITICAL
    3. 日志的内容: (可以套用其他程序的log文件的内容)
    4. 内容基本上可以为: 年月日,时分秒,级别,代码行,具体报错
"""


def logger(fileLog=True, name=__name__):
    logDir = checkFilePath("../logs/logInfoCheck.log")
    # 1. 创建一个日志收集器对象
    logObj = logging.getLogger(name)

    # 2. 设置日志的级别
    logObj.setLevel(logging.INFO)

    # 3. 设定日志的内容格式
    fmt = "%(asctime)s - %(levelname)s - %(filename)s[%(lineno)d]: %(message)s"
    formatter = logging.Formatter(fmt)

    # 4. 设定日志输出渠道
    if fileLog:  # "文件输出"的模式
        handle = logging.FileHandler(logDir, encoding='utf-8')
        # 日志内容与渠道进行绑定
        handle.setFormatter(formatter)
        # 把日志对象与渠道进行绑定
        logObj.addHandler(handle)
    else:    # "控制台输出"的模式
        handle = logging.StreamHandler()
        # 日志内容与渠道进行绑定
        handle.setFormatter(formatter)
        # 把日志对象与渠道进行绑定
        logObj.addHandler(handle)
    return logObj


log = logger()

if __name__ == '__main__':
    log = logger()  # 控制台输出日志
    log.info('我是日志信息')


