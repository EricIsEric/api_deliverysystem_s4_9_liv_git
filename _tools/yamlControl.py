# -*- coding: utf-8 -*-
"""
# 当前文件: yamlControl.py
# 创建日期: 2021/7/26 20:30
# 创建作者: Eric
"""
import yaml


def get_yaml_data(fileDir):
    # 1. 把文件从磁盘加载到内存--打开
    with open(fileDir, 'r', encoding='utf-8') as fo:
        # 2. 使用yaml来读取
        res_1 = yaml.load(fo, Loader=yaml.FullLoader)  # Loader参数实现取消输出的警告信息
        print(res_1)
        # print(res_1['Uname_Pwd']['password'])  # 实现嵌套字典的取值


def get_yamls_data(fileDir):  # 多段yaml读取
    resList = []
    # 1. 把文件从磁盘加载到内存--打开
    with open(fileDir, 'r', encoding='utf-8') as fo:
        # 2. 使用yaml来读取
        res_1 = yaml.load_all(fo, Loader=yaml.FullLoader)  # Loader参数实现取消输出的警告信息

        # 设定返回数据的样式,这里根据具体的项目来,数据驱动需要什么样式就封装成什么样式
        for one in res_1:
            resList.append(one)
        return resList


# yaml的写入操作
def set_yaml_data(fileDir):
    with open(fileDir, 'w', encoding='utf-8') as fo:
        dict1 = {'b': 'Eric3'}
        yaml.dump(dict1, fo)


# 处理用例数据
def get_yaml_caseData(fileDir):
    resList = []
    # 1. 把文件从磁盘加载到内存--打开
    with open(fileDir, 'r', encoding='utf-8') as fo:
        # 2. 使用yaml来读取
        res_1 = yaml.load(fo, Loader=yaml.FullLoader)  # Loader参数实现取消输出的警告信息
        del res_1[0]  # 下标为0的位置的数据是用来做全局配置的数据,使用时可以移除

        # 设定返回数据的样式,这里根据具体的项目来,数据驱动需要什么样式就封装成什么样式
        for one in res_1:
            resList.append((one['detail'], one['data'], one['resp']))
        return resList


if __name__ == '__main__':
    set_yaml_data('../configs/config2.yaml')
    print("Write-OK!")
