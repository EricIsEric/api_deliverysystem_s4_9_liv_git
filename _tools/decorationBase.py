# -*- coding: utf-8 -*-
"""
# 当前文件: decorationBase.py
# 创建日期: 2021/8/1 22:08
# 创建作者: Eric
"""
import time


# ------------------------------- 装饰器技术与项目结合进行使用**开始 ------------------------------- #
"""
实际项目代码里是有:
    1. 参数
        看你把show_time装饰器放到哪一个函数前,看这个函数的参数个数
        如果出现不定数量,我们就得在inner里添加可变数量标识*args, 如果是元组就加**kwargs
    2. 返回值的
        看你把show_time装饰器放到哪一个函数前,看这个函数的返回值
        如果出现不定数量,我们就得在inner里添加可变数量标识*args, 如果是元组就加**kwargs
        我们就得在def inner(*args)h函数里使用res来接收: res = func(*args)
        然后就将res返回出去: return res
"""


def show_time(func):
    def inner(*args):  # 因为项目中参数个数不定,所以这里使用*args写法
        startTime = time.time()
        res = func(*args)  # 不定参数传入
        endTime = time.time()
        print("这个用例执行耗时: ", endTime - startTime)
        return res
    return inner


@show_time
def test(a, b, c):
    print("-----自动化开始执行-----")
    time.sleep(2)
    print(a + b + c)
    print("-----自动化结束执行-----")
# ------------------------------- 装饰器技术与项目结合进行使用**结束 ------------------------------- #


if __name__ == '__main__':
    test(100, 200, 300)  # 实现了不定参数的求和



