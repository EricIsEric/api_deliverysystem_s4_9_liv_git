# encoding=utf-8
"""
# 当前文件: excelControl
# 当前日期: 2021/7/19 11:50
# 当前作者: Eric
"""
import xlrd
import json

"""
目标: 读取excel数据
方案: 
    1. 通过指定的行号列号去获取对应的用例数据---for循环行号读取数据
        缺点: 如果测试人员对用例进行修改了,会导致代码报错
    2. 智能获取,也即模糊匹配
    
xlrd: 操作.xls格式的文件
openpyxl: xlsx文件
pandas: 大数据CSV
"""


def getExcelData(set_ExcelDir, set_SheetName, set_CaseName):
    resList = []  # 存放Excel读取的结果, 放在函数体内,形成局部变量,每一次调用,这个list都会被清空一次,避免被累加

    # 1. 获取Excel路径
    excelDir = set_ExcelDir

    # 2. 把excel加载到内存 -- open,  formatting_info保持原样式
    workBook = xlrd.open_workbook(excelDir, formatting_info=True)

    # 3. 获取对应的sheet表
    # print(workBook.sheet_names())  # 获取所有的sheet的名称
    workSheet = workBook.sheet_by_name(set_SheetName)

    # # 4. 获取一行数据
    # print(workSheet.row_values(0))
    #
    # # 5. 获取一列数据
    # print(workSheet.col_values(0))
    #
    # # 6. 获取单元格的数据 (如1行9列)
    # print(workSheet.cell(1, 9).value)

    # 开始模糊匹配第0列数据
    rowsNum = 0  # 遍历变量表示行数
    for one in workSheet.col_values(0):
        if set_CaseName in one:
            reqBodyData = workSheet.cell(rowsNum, 9).value  # 请求体---字符串
            respondData = workSheet.cell(rowsNum, 11).value  # 响应体---字符串

            # 接口需要传递的是字典格式数据,需要将excel读取到的字符串格式转换为字典格式--json.loads()转字典
            # 把以上循环获取到的并且格式转换之后的值依次存储起来
            resList.append((json.loads(reqBodyData), json.loads(respondData)))  # 列表里存储元组

        rowsNum += 1
    return resList


def fillInResult(set_ExcelDir, set_SheetName):
    workBook = xlrd.open_workbook(set_ExcelDir, formatting_info=True)
    workSheet = workBook.sheet_by_name(set_SheetName)
    print(f"##########{workSheet.cell(1, 12).value}")
    workSheet.cell(1, 12).value = "Pass"


def fillInResult_v2(set_ExcelDir, set_SheetName, set_Rows):
    workBook = xlrd.open_workbook(set_ExcelDir, formatting_info=True)
    workSheet = workBook.sheet_by_name(set_SheetName)
    print(workSheet.cell(set_Rows, 12).value)
    workSheet.cell(1, 12).value = "TEST"


if __name__ == '__main__':
    res = getExcelData("../data/ApiCases_v1.0.xls", "登录模块", "Login")
    for items in res:
        print(items)


# 如果需要获取其他列的数据, 在函数形参里添加一个可变数量参数
