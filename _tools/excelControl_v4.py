# encoding=utf-8
"""
# 当前文件: excelControl
# 当前日期: 2021/7/19 11:50
# 当前作者: Eric
"""
import xlrd
import json
from commons._BaseFuncs import checkFilePath

"""
问题反馈:
    1. 对是json字符串的数据需要转换为字典
    2. 如果不是json字符串数据,还是以本身字符串形式来表达
解决方案:
    1. 在获取到具体的单元格数据的时候,需要判断是不是json
"""


def getExcelData(set_SheetName, set_CaseName, *args, runCase=None, set_ExcelDir=checkFilePath('../data/ApiCases_v2.0.xls')):
    if runCase is None:
        runCase = ['All']

    resList = []  # 存放Excel读取的结果, 放在函数体内,形成局部变量,每一次调用,这个list都会被清空一次,避免被累加

    # 1. 获取Excel路径
    excelDir = set_ExcelDir

    # 2. 把excel加载到内存 -- open,  formatting_info保持原样式
    workBook = xlrd.open_workbook(excelDir, formatting_info=True)

    # 3. 获取对应的sheet表
    # print(workBook.sheet_names())  # 获取所有的sheet的名称
    workSheet = workBook.sheet_by_name(set_SheetName)

    # 通过用户输入的想获取的列名解析出对应的列下标
    # 用户传入列名字符串,但是代码里处理的是列编号
    # 标题  URL
    colIdx = []  # 存放下标的列表
    for i in args:  # 遍历元组元素
        # 列名在第一行数据里有
        colIdx.append(workSheet.row_values(0).index(i))

# --------------解析用户筛选用例
    runList = []
    if 'All' in runCase:  # 全部执行,不做处理
        runList = workSheet.col_values(0)
    else:
        for one in runCase:
            if '-' in one:
                start, end = one.split("-")
                for i in range(int(start), int(end)+1):
                    runList.append(set_CaseName + f'{i:0>3}')
            else:
                runList.append(set_CaseName + f'{one:0>3}')

    # 开始模糊匹配第0列数据
    rowsNum = 0  # 遍历变量表示行数
    for one in workSheet.col_values(0):
        if set_CaseName in one and one in runList:
            getColData = []
            for num in colIdx:
                tmp = workSheet.cell(rowsNum, num).value
                if is_Json(tmp):  # 判断到是json格式就转换
                    tmp = json.loads(tmp)
                getColData.append(tmp)
            resList.append(getColData)

        rowsNum += 1
    return resList


# ---------------------------------------------------------------------------------
def is_Json(inStr):
    try:
        json.loads(inStr)
    except:
        return False
    return True
# ---------------------------------------------------------------------------------


if __name__ == '__main__':
    res = getExcelData("登录模块", "Login", "URL", "请求参数", "响应预期结果")
    for items in res:
        print(items)
