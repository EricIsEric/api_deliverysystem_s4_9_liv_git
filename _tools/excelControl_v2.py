# encoding=utf-8
"""
# 当前文件: excelControl
# 当前日期: 2021/7/19 11:50
# 当前作者: Eric
"""
import xlrd
import json

"""
目标: 如果需要获取其他列的数据, 在函数形参里添加一个可变数量参数
方案: 
    1. 使用列下标,可读性不好
    2. 通过列名获取
    
xlrd: 操作.xls格式的文件
openpyxl: xlsx文件
pandas: 大数据CSV
"""


def getExcelData(set_ExcelDir, set_SheetName, set_CaseName, *args):
    resList = []  # 存放Excel读取的结果, 放在函数体内,形成局部变量,每一次调用,这个list都会被清空一次,避免被累加

    # 1. 获取Excel路径
    excelDir = set_ExcelDir

    # 2. 把excel加载到内存 -- open,  formatting_info保持原样式
    workBook = xlrd.open_workbook(excelDir, formatting_info=True)

    # 3. 获取对应的sheet表
    # print(workBook.sheet_names())  # 获取所有的sheet的名称
    workSheet = workBook.sheet_by_name(set_SheetName)

    # 通过用户输入的想获取的列名解析出对应的列下标
    # 用户传入列名字符串,但是代码里处理的是列编号
    # 标题  URL
    colIdx = []  # 存放下标的列表
    for i in args:  # 遍历元组元素
        # 列名在第一行数据里有
        colIdx.append(workSheet.row_values(0).index(i))

    # 开始模糊匹配第0列数据
    rowsNum = 0  # 遍历变量表示行数
    for one in workSheet.col_values(0):
        if set_CaseName in one:
            getColData = []
            for num in colIdx:
                tmp = workSheet.cell(rowsNum, num).value
                getColData.append(tmp)
            resList.append(getColData)

        rowsNum += 1
    return resList


def fillInResult(set_ExcelDir, set_SheetName, *args):
    workBook = xlrd.open_workbook(set_ExcelDir, formatting_info=True)
    workSheet = workBook.sheet_by_name(set_SheetName)
    print(f"##########{workSheet.cell(1, 12).value}")
    workSheet.cell(1, 12).value = "Pass"


def fillInResult_v2(set_ExcelDir, set_SheetName, set_Rows):
    workBook = xlrd.open_workbook(set_ExcelDir, formatting_info=True)
    workSheet = workBook.sheet_by_name(set_SheetName)
    print(workSheet.cell(set_Rows, 12).value)
    workSheet.cell(1, 12).value = "TEST"


if __name__ == '__main__':
    res = getExcelData("../data/ApiCases_v1.0.xls", "登录模块", "Login", "标题", "URL", "实际结果")
    for items in res:
        print(items)
