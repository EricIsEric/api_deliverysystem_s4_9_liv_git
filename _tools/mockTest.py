# -*- coding: utf-8 -*-
"""
# 当前文件: mockTest.py
# 创建日期: 2021/7/28 20:39
# 创建作者: Eric
"""
import requests
import time
import threading

HOST = 'http://127.0.0.1:9999'

# url = f'{HOST}/Eric_test_01'
# rsp = requests.get(url)
# print(rsp.text)


# url = f'{HOST}/Eric_test_05'
# payload = {"key1": "abc"}
# rsp = requests.post(url, data=payload)
# print(rsp.text)


# ==============================异步接口演示开始============================== #
# 以下新增一个异步接口的场景:
"""
1. 店铺向管理平台申请退订订单业务
2. 管理平台接受了店铺的申请并且向店铺返回一个退订流程id号
3. 平台进行处理申请
4. 店铺使用这个流程id号去查询之前申请的退订业务是否完成
"""


# Step1. 写服务器端---使用mock技术
# Step2. 写测试代码

# 提交申请接口
def create_order():
    url = f'{HOST}/api/order/create/'
    payload = {"user_id": "Eric_001", "goods_id": "1234", "num": 1, "amount": 100.8}
    rsp = requests.post(url, json=payload)
    return rsp.json()['order_id']


# 查询接口
# 概念: 接口查询频率 + 接口超时时间
def get_order_result(orderId, interval=3, timeout=20):
    """
    :param orderId: 订单的id
    :param interval: 频率, 默认3秒一次
    :param timeout: 默认20秒超时
    :return: 返回查询结果
    """

    url = f'{HOST}/api/order/get_result001/'
    payload = {"order_id": orderId}
    # 1. 查询开始计时
    start_time = time.time()

    # 2. 查询结束时间
    end_time = start_time + timeout

    req_count = 0  # 计算查询次数
    while time.time() < end_time:
        rsp = requests.get(url, params=payload)
        req_count += 1

        if rsp.text:
            print(f"当前是第{req_count}次查询,结果是--->{rsp.text}")
            break  # 在超时的范围内有返回值就立即退出循环
        else:
            print(f"当前是第{req_count}次查询,结果是--->订单没有处理完成, 请稍后再查询!")
        time.sleep(interval)
    print("-------查询结束-------")
    # noinspection PyUnboundLocalVariable
    return rsp.text


if __name__ == '__main__':
    Ord_id = create_order()
    print(Ord_id)

    # res = get_order_result(Ord_id)

    """
    语法: threading.Thread(target=希望被做成子线程的现有的函数名, args=以元组的形式传入这个函数所需要的参数)
    
    子线程与主线程的运行逻辑:
        1. 如果主线程结束,子线程会被强行结束
            这时就需要将子线程设置成守护线程
        2. 主线程结束之前,如果子线程还未结束,主线程会等待子线程结束之后才会结束
            join()
        
    """
    AllStartTime = time.time()
    # 1. 创建子线程
    t1 = threading.Thread(target=get_order_result, args=(Ord_id,))  # 单个逗号与参数一起用圆括号包围起来表示一个但元素的元组

    # 2. 守护线程, 当主线程退出时,这个子线程就会退出
    t1.setDaemon(True)

    # 3. 启动线程
    t1.start()

    # 前面接口查询出错时会连续等待7次,后面的代码会被阻塞不能执行,此时虽说是异步,但实际上仍然是同步的操作
    # 可以考虑使用多线程来实现异步的效果

    for one in range(20):
        time.sleep(1)  # 模拟其它接口的执行过程
        print(f"我正在执行其它模块的自动化测试! --> {one+1}")

    AllEndTime = time.time()
    print(f"-----整个程序运行用时: {AllEndTime-AllStartTime}")

    # 使用"多线程"/"协程"/"协程+进程"/"异步框架"/"分布式" 技术来实现
    # 需求点: 希望提高自动化的执行效率---此处采用"多线程"来实现,比较简单
    # 希望在等待查询结果的同时,可以去执行其它的自动化测试
    """
    可以把查询接口做成子线程
    """

    # 带有很多sleep的IO密集型的代码对于多线程和分布式很有效果
    # 对于计算密集型的代码,多线程和分布式不一定会比直接运行快
# ==============================异步接口演示结束============================== #
